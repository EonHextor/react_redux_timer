import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import { Provider } from "react-redux";
import store from "./Components/Store/Store";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  // Стор проекта переданий всім елементам за допомогою Provider Redux
  <Provider store={store}>
    <App />
  </Provider>
);
