// Початковий стан редюсера
const initialState = {
  minutes: localStorage.getItem("minutes")
    ? +localStorage.getItem("minutes")
    : 0,
  secondes: localStorage.getItem("secondes")
    ? +localStorage.getItem("secondes")
    : 0,
  sound: null,
  start: null,
  launch: false,
  modal: false,
};

// Редюсер проекта
const TimerReducer = (state = initialState, action) => {
  switch (action.type) {
    case "SetTimer":
      return {
        ...state,
        minutes: +action.payload.minutes,
        secondes: +action.payload.secondes,
      };
    case "StartTimer":
      return {
        ...state,
        minutes:
          state.minutes !== 0 && state.secondes < 1
            ? state.minutes - action.payload.minutes
            : state.minutes,
        secondes:
          state.secondes !== 0
            ? state.secondes - action.payload.secondes
            : state.secondes === 0 && state.minutes !== 0
            ? 59
            : state.secondes === 0 && state.minutes === 0
            ? 0
            : state.secondes - action.payload.secondes,
        start: action.payload.start,
      };
    case "StopTimer":
      return { ...state, launch: action.payload };
    case "SoundCheck":
      return { ...state, sound: action.payload };
    case "Modal":
      return { ...state, modal: action.payload };
    default:
      return state;
  }
};

export default TimerReducer;
