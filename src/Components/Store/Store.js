import { createStore } from "redux";
import TimerReducer from "../TimerReducer/TimerReducer";

// Стор проекта
const store = createStore(TimerReducer)

export default store;